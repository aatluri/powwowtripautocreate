from trip_parser import TripParser
from bs4 import BeautifulSoup
import pprint
import json
import re


class EaseMyTripParser(TripParser):
    def __init__(self, text,attachments):
        super(EaseMyTripParser, self).__init__(text,attachments)
        try:
            self._make_html_soup()
        except Exception as e:
            logging.error(e.message)
        else:
            self._construct_response()

    def _make_html_soup(self):
        if self.html_text:
            table_text = '<table width="650" border="1" cellspacing="0" cellpadding="0" style="margin:0'
            content_table = self.html_text[self.html_text.find(table_text):]
            self.html_sp = BeautifulSoup(content_table, 'html.parser').findAll("table", {"style": "font-size:12px;"})
            if not self.html_sp:
                self.html_sp = BeautifulSoup(content_table, 'html.parser').findAll("table", {
                    "style": "font-size:12px"
                })

    def _get_passenger_info(self):
        passengers = []
        info_cols = self.html_sp[0].findAll("td", {"valign": "middle"})
        passenger_name_cols = info_cols[5::4]

        for entry in passenger_name_cols:
            last_name, misc = entry.text.split('/', 1)
            last_name = last_name.strip()
            name_parts = [i.strip() for i in misc.split() if not i.startswith('(')]
            salutation = name_parts.pop()
            first_name = " ".join(name_parts)
            passengers.append(self._get_passenger_object(first_name, last_name, salutation))
        return passengers

    def _get_location_info(self):
        loc_info = self.html_sp[1].find("td", {"height": "30", "colspan": "6"})
        loc_info.find("span").extract()
        from_loc, to_loc = loc_info.text.split(' To ', 1)

        time_info = self.html_sp[1].findAll("td", {"width": "31%"})
        departure_time = time_info[0].findAll("span")[-1].text.split()
        arrival_time = time_info[1].findAll("span")[-1].text.split()
        depdate=departure_time[1].split("/")
        arrdate=arrival_time[1].split("/")
        dep_date="20"+depdate[2]+"-"+depdate[1]+"-"+depdate[0]
        arr_date="20"+arrdate[2]+"-"+arrdate[1]+"-"+arrdate[0]
        departure_time = dep_date + " " + departure_time[3]+":00"
        arrival_time = arr_date + " " + arrival_time[3]+":00"
        return [self._get_location_object(from_loc, '', departure_time)], \
               [self._get_location_object(to_loc, '', arrival_time)]

    def _get_reservation_info(self):
        res_info = self.html_sp[1].findAll("td", {"valign": "middle"})[1]  # pnr number
        return [res_info.text.split('-')[-1]]

    def _get_carrier_info(self):
        carrier_info = str(self.html_sp[1].find("img", id=re.compile("Img9$")))
        carrier_parts = carrier_info.split("<br>")
        carrier_name = carrier_parts[1]
        carrier_number = carrier_parts[2].split('<')[0]
        return [self._get_carrier_object(carrier_name, carrier_number)]
