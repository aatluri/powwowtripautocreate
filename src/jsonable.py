"""
The idea of the JSONable objects is to automatically convert python classes to a JSON object
The key for the JSON object is the name of property and the value is the value, but it needs to
be one of the following types:
string_type: str, unicode
numeric_type: int, float, long

"""


class JSONable(object):
    @staticmethod
    def list_type(values):
        assert isinstance(values, (list, set, tuple,))
        accepted_types = (str, unicode, int, float, long, JSONable,)
        for val in values:
            assert isinstance(val, accepted_types), "ERROR: values in the list should be of type: %s, found %s" % (
                str(accepted_types), type(val)
            )
        return values

    @staticmethod
    def string_type(value):
        assert isinstance(value, (str, unicode,)), "ERROR: Expecting type str or unicode, found: %s" % type(value)
        return value

    @staticmethod
    def numeric_type(value):
        assert isinstance(value, (int, float, long,)), "ERROR: Expecting type int or float or long, found: %s" % \
                                                       type(value)
        return value

    @staticmethod
    def jsonable_type(value):
        assert isinstance(value, JSONable), "ERROR: Expecting type of JSONable, found: %s" % type(value)
        return value

    def _jsonable(self, value):
        """
        This method converts the incoming value into a type that can be processed by json.dumps method.
        Anything that is a string(str, unicode), number(int, float, long) or None is left as is
        If the object is of type JSONable the _collect_attributes method is called on it
        If the object is a map, or a list it is iterated and converted to a json map or list accordingly
        All other unrecognised objects are assigned to None.

        :param value:
        :return: a jsonable value
        """
        if isinstance(value, JSONable):
            val = value._collect_attributes()
        elif isinstance(value, (list, tuple, set,)):
            vals = []
            for val in value:
                vals.append(self._jsonable(val))
            val = vals
        elif isinstance(value, dict):
            vals = {}
            for k, v in value.iteritems():
                vals[str(k)] = self._jsonable(v)
            val = vals
        elif isinstance(value, (str, unicode, int, float, long)):
            val = value
        else:
            val = None
        return val

    def _collect_attributes(self):
        op_dict = {}
        for key, value in self.__dict__.iteritems():
            op_dict[key] = self._jsonable(value)
        return op_dict

    def get_jsonable_object(self):
        return self._collect_attributes()
