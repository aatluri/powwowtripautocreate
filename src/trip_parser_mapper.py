from clear_trip_parser import ClearTripParser
from ease_my_trip_parser import EaseMyTripParser
from worldmate_trip_parser import WorldMateParser

TRIP_PARSER_MAPPER = {
    "no-reply@cleartrip.com": ClearTripParser,
    "care@easemytrip.com": EaseMyTripParser,
    "noreply@worldmatelive.com": WorldMateParser
}

