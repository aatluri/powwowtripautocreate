import sys
import imaplib
import getpass
import email
import email.header
import datetime
import json
import pprint
import smtplib
from email.MIMEText import MIMEText
import uuid
import base64
import quopri
from StringIO import StringIO
import re
import traceback
import os
from powwow_api_db_mgmt import get_customer_access_token
from powwow_api_db_mgmt import get_airport_code
from powwow_api_db_mgmt import call_add_trip_api
from trip_parser_mapper import TRIP_PARSER_MAPPER
from email_mgmt import read_tripmailbox, synchornoussendemail
from email_mgmt import EmailManagement

TRIPFAIL="tripfail@powwowtravelapp.com"
POWWOWEMAIL="trip@powwowtravelapp.com"
WORLDMATEREPLYEMAIL="noreply@worldmatelive.com"

def extract_email_contents(email_content):
    """ Extracts the contents of the email being parsed
    
    Args:
        email_content: The content of the email message being parsed
    
    Returns:
        htmlbody: The html content of the email
        attachments: A List consiting of the attachments in the email
    
    """
    print "extracting email contents"
    plaintextbody=""
    htmlbody=""
    attachments=[]
    emsg = email.message_from_string(email_content)
    customeremail = emsg.get("From")[emsg.get("From").index("<")+1:emsg.get("From").index(">")]
    for part in emsg.walk():
        attachment = parse_attachment(part)
        if attachment:
            attachments.append(attachment)
        elif part.get_content_type() == "text/plain":
            if plaintextbody is None:
                plaintextbody = ""
            plaintextbody += unicode(part.get_payload(decode=True),part.get_content_charset(),'replace').encode('utf8','replace')
        elif part.get_content_type() == "text/html":
            if htmlbody is None:
                htmlbody = ""
            htmlbody += unicode(part.get_payload(decode=True),part.get_content_charset(),'replace').encode('utf8','replace')
    
    #print plaintextbody
    #print htmlbody
    #print attachments
    return htmlbody,attachments


def parse_attachment(message_part):
    """ Parses the attachments in the email message
    
    Args:
        message_part: The part of the email message being parsed
    
    Returns:
        Returns the content of the message part if its an attachment
    
    """
    print "parsing attachment"
    content_disposition = message_part.get('Content-Disposition')
    if content_disposition:
        disposition=content_disposition.strip().split(";")
        if (disposition[0].lower() == "attachment"):
            attachment = message_part.get_payload(decode=True)
            #print attachment
            return attachment
    return None


def extract_travelcompanyemail(htmlbody,customeremail,emailmessage):
    """ Extracts the travel company's email from the email message
    
    Args:
        htmlbody: The html content of the email message being parsed
        customeremail: The attachments in the email being parsed
        emailmessage: The entire email message being parsed
    
    Returns:
        Returns the travelcompany's email or  "" if an error occurs.
    
    """
    print "Extracting travelcompanyemail"
    travelcompanyemail=[]
    emsg = email.message_from_string(emailmessage)
    fromemail = emsg.get("From")[emsg.get("From").index("<")+1:emsg.get("From").index(">")]
    print fromemail
    if fromemail==WORLDMATEREPLYEMAIL:
        return "1","noreply@worldmatelive.com"
    travelcompanyemail = re.findall("(?:[a-z0-9!#$%&'*+\\\\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\\\\/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",htmlbody) # regex that finds all the email addresses in the html body of the email
    travelcompanyemailset=set(travelcompanyemail)
    travelcompanyemail=list(travelcompanyemailset)
    try:
        travelcompanyemail.remove(customeremail) #From the list that the regex returns, we remove the customers email
        travelcompanyemail.remove(POWWOWEMAIL) #From the list that the regex returns, we remove the powwow email
    except ValueError as e:
        pass
    print travelcompanyemail
    try:       
        for trvlemail in travelcompanyemail:
            print trvlemail
            if trvlemail in os.environ['WORLDMATEBLOCKLIST']:
                return "0",trvlemail
            if trvlemail in TRIP_PARSER_MAPPER:
                return "1",trvlemail
        return "2",""
    except Exception as e:
        print "Unexpected error while extracting travelcompanyemail: ", str(e)
        traceback.print_exc()
        return ""



def extract_validate_customeremail(email_content,attachments,UUID):
    """ Extracts the customer's email from the email message
    
    Args:
        email_content: The content of the email message being parsed
        attachments: The attachments in the email being parsed
        uuid: The unique identifier which can be used to troubleshoot any issues later on
    
    Returns:
        access_token: The facebook access token of the user
        customeremail: The email address of the customer
    
    """
    print "extracting and validating customer email"
    emsg = email.message_from_string(email_content)
    customeremail = emsg.get("From")[emsg.get("From").index("<")+1:emsg.get("From").index(">")] # For an internally parsed email, customer email will be in the From header
    if customeremail==WORLDMATEREPLYEMAIL: #For a worldmate repsonse, then the customer email will be in the attachment where we placed it when sending the trip confirmation email to worldmate.
        attachment=attachments[0]
        customeremail=attachment[attachment.index("CUSTEMAILSTART:")+len("CUSTEMAILSTART:"):attachment.index("CUSTEMAILEND:")]
    access_token=get_customer_access_token(customeremail) #check if the customeremail exists in the powwow database. If yes return true. if not return false
    print "access_token=" + str(access_token)
    if access_token=="":
        print "Customer Email " + customeremail+ " did not exist in the powwow database"
        return None,customeremail
    return access_token,customeremail


def check_worldmate_responsefailure(emailmessage,customeremail,UUID):
    """ Checks the email message being read if its a worldmate failure to parse response.
    
    Args:
        emailmessage: The emailmessage being parsed
        customeremail: The email address of the customer
        uuid: The unique identifier which can be used to troubleshoot any issues later on
    
    Returns:
        None or True
    
    """
    print "Extracting worldmate response Subject to check for failure"
    emsg = email.message_from_string(emailmessage)
    subject = emsg.get("Subject")
    fromemail = emsg.get("From")[emsg.get("From").index("<")+1:emsg.get("From").index(">")]
    if "Email was not parsed" in subject and fromemail==WORLDMATEREPLYEMAIL:
        print "WorldMate was not able to parse Trip for user : " + customeremail
        return None
    return True









