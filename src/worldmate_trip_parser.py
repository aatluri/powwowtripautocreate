# -*- coding: utf-8 -*-
from trip_parser import TripParser
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import logging
import traceback


class WorldMateParser(TripParser):
    def __init__(self, text,attachments):
        super(WorldMateParser, self).__init__(text,attachments)
        try:
            self._make_xml()
        except Exception as e:
            print "Unexpected error: ", str(e)
            traceback.print_exc()
        else:
            self._construct_response()

    def _make_xml(self):
        if self.attachment:
            root = ET.fromstring(self.attachment)
            items= root.find("items")
            flights = items.findall("flight")   
            self.xml_flights_node=flights

    def _get_passenger_info(self):
        if not self.attachment:
            return
        passenger_list = []
        return passenger_list

    def _get_location_info(self):
        if not self.attachment:
            return
        
        from_locations = []
        to_locations = []
        
        for flight in self.xml_flights_node:
            departure = flight.find("departure")
            from_loc = ""
            from_loc_code=departure.find("airport-code").text
            departure_time=departure.find("local-date-time").text
            arrival = flight.find("arrival")
            to_loc = ""
            to_loc_code=arrival.find("airport-code").text
            arrival_time=arrival.find("local-date-time").text
            from_locations.append(self._get_location_object(from_loc, from_loc_code, departure_time))
            to_locations.append(self._get_location_object(to_loc, to_loc_code , arrival_time))
        return from_locations, to_locations

    def _get_reservation_info(self):
        if not self.attachment:
            return
        
        pnrs = []
        for flight in self.xml_flights_node:
            pnrdetail=flight.find("provider-details")
            pnrs.append(pnrdetail.find("confirmation-number").text)
        return pnrs

    def _get_carrier_info(self):
        if not self.attachment:
            return

        carriers = []
        for flight in self.xml_flights_node:
            carriernameinfo = flight.find("provider-details")
            carrier_name=carriernameinfo.find("name").text
            carriernumberinfo = flight.find("details")
            carrier_number=carriernumberinfo.get("airline-code")+carriernumberinfo.get("number")
            if not carrier_name: carrier_name=""
            if not carrier_number: carrier_number=""
            carriers.append(self._get_carrier_object(carrier_name, carrier_number))
        return carriers
