import sys
import imaplib
import email
import email.header
import datetime
import json
import pprint
import smtplib
from email.MIMEText import MIMEText
import uuid
import threading
import time

SEND_EMAIL_ACCOUNT = "powwow@powwowtravelapp.com"
READ_EMAIL_ACCOUNT = "trip@powwowtravelapp.com"
PASSWORD = "powwow8083"




class EmailManagement(threading.Thread):
    def __init__(self, threadID, name, to_addr, message, subject, uuid):
        """ Creates a new thread
    
        Args:
            threadID: The ID of the current executing thread
            name: The name of the thread
            to_addr: The email address to which the email needs to be sent
            message: The content of the email to be sent
            subject: The subject of the email to be sent
            uuid: The unique identifier which can be used to troubleshoot any issues later on

        Returns:
            Nothing

        """
        threading.Thread.__init__(self)
        self.threadID = str(threadID)
        self.name=name
        self.to_addr = to_addr
        self.message = message
        self.subject = subject
        self.uuid = uuid
    def run(self):
        print "Starting " + self.threadID
        sendemailasync(self.name,self.to_addr, self.message, self.subject, self.uuid)
        print "Exiting " + self.threadID


#Note: All the below methods are outside of the above class
def sendemailasync(threadname, to_addr, message, subject,uuid):
    """ Asynchronously sends an email
    
    Args:
        threadname: The name of the current executing thread
        to_addr: The email address to which the email needs to be sent
        message: The content of the email to be sent
        subject: The subject of the email to be sent
        uuid: The unique identifier which can be used to troubleshoot any issues later on
    
    Returns:
        Nothing
    
    """
    print "%s: %s" % (threadname, time.ctime(time.time()))
    print threadname + ": Sending Email to: " + to_addr + " with Subject=" + subject
    smtp_host = "smtp.gmail.com"
    smtp_port = 587
    # create a Message instance from the email data
    message = email.message_from_string(message)

    # replace headers (could do other processing here)
    # message.replace_header("From", SEND_EMAIL_ACCOUNT)
    message.replace_header("To", to_addr)
    if subject !="" and str(uuid)!="":
        message.replace_header("Subject", subject + " ;CaseID="+str(uuid))
    # open authenticated SMTP connection and send message with
    # specified envelope from and to addresses
    smtp = smtplib.SMTP(smtp_host, smtp_port)
    smtp.ehlo()
    smtp.starttls()
    smtp.login(READ_EMAIL_ACCOUNT, PASSWORD)
    smtp.sendmail(READ_EMAIL_ACCOUNT, to_addr, message.as_string())
    smtp.quit()
    print threadname + ": Email Sent Successfully"


def process_mailbox(M):
    """ Processes each email in the mailbox
    
    Args:
        M: The mailbox to be processed
    
    Returns:
        rv: A return value
        emailmessages: A List of email messages
    
    """
    emailmessages=[]
    rv, data = M.search(None, '(UNSEEN)')
    if rv != 'OK' or data==[""]:
        print "No messages found!"
        return rv,emailmessages
    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            print "ERROR getting message", num
            return rv,emailmessages
        emailmessages.append(data[0][1])
        msg = email.message_from_string(data[0][1])
        decode = email.header.decode_header(msg['Subject'])[0]
        subject = unicode(decode[0])
        print 'Message %s: %s' % (num, subject)
        print 'Raw Date:', msg['Date']
    return rv,emailmessages



def read_tripmailbox(folder):
    """ Reads the respective folder of the powwow email account
    
    Args:
        folder: The folder in the powwow email account to be read
    
    Returns:
        emailmessages: A List of email messages
    
    """
    emailmessages=[]
    M = imaplib.IMAP4_SSL('imap.gmail.com')
    try:
        rv, data = M.login(READ_EMAIL_ACCOUNT, PASSWORD)
    except imaplib.IMAP4.error as e:
	    print "LOGIN FAILED for mailbox: " + folder + " for mailbox: " + READ_EMAIL_ACCOUNT

    print rv, data

	#rv, mailboxes = M.list()
	#if rv == 'OK':
	#    print "Mailboxes:"
	#    print mailboxes

    rv, data = M.select(folder)
    if rv == 'OK':
        print "Processing mailbox " + folder
        rv,emailmessages=process_mailbox(M)
        M.close()
    else:
		print "ERROR: Unable to open mailbox ", rv
    M.logout()
    return emailmessages




def synchornoussendemail(to_addr, message, subject,uuid):
    """ Synchronously sends an email -- This method is currently not used in the code base
    
    Args:
        to_addr: The email address to which the email needs to be sent
        message: The content of the email to be sent
        subject: The subject of the email to be sent
        uuid: The unique identifier which can be used to troubleshoot any issues later on
    
    Returns:
        Nothing
    
    """
    print "Sending Email to: " + to_addr + " with Subject=" + subject
    smtp_host = "smtp.gmail.com"
    smtp_port = 587
    # create a Message instance from the email data
    message = email.message_from_string(message)

    # replace headers (could do other processing here)
    # message.replace_header("From", SEND_EMAIL_ACCOUNT)
    message.replace_header("To", to_addr)
    if subject !="" and str(uuid)!="":
        message.replace_header("Subject", subject + " ;CaseID="+str(uuid))
    # open authenticated SMTP connection and send message with
    # specified envelope from and to addresses
    smtp = smtplib.SMTP(smtp_host, smtp_port)
    smtp.ehlo()
    smtp.starttls()
    smtp.login(READ_EMAIL_ACCOUNT, PASSWORD)
    smtp.sendmail(READ_EMAIL_ACCOUNT, to_addr, message.as_string())
    smtp.quit()
    print "Email Sent Successfully"










