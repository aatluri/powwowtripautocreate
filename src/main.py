
import email
import base64
import quopri
import json
from StringIO import StringIO
import re
import time
import requests
import traceback
import uuid
import datetime
import threading
import os


from trip_parser_mapper import TRIP_PARSER_MAPPER
from powwow_api_db_mgmt import get_customer_access_token,get_airport_code,call_add_trip_api
from email_mgmt import read_tripmailbox, synchornoussendemail
from email_content_parser import extract_email_contents,parse_attachment,extract_travelcompanyemail,extract_validate_customeremail, check_worldmate_responsefailure
from email_mgmt import EmailManagement






UNABLE_TO_RETREIVE_TRIP_DETAILS = "Error: Unable to parse trip details from the email contents"
UNKNOWN_SENDER_EMAIL = "Error : Unknown email sender. Unable to parse email"

WORLDMATEEMAIL="17199096-4bd8-4392-8ae5-4b241b7949f7-5387@api.worldmate.com"
POWWOWEMAIL="trip@powwowtravelapp.com"
TRIPFAIL="tripfail@powwowtravelapp.com"
UUID=uuid.uuid4()
threads=[]


#def get_access_token()



def get_trip_details(travelcompanyemail,htmlbody,access_token,customeremail,attachments,emailmessage):
    """ Parses the details of the trip from the contents of the email
    
    Args:
        travelcompanyemail: The email of the travelcompany with which the customer booked his/her travel
        htmlbody: The html portion of the email
        access_token: The facebook access_token of the customer. This is used when calling the powwow trip creation API
        customeremail: The customer's email
        attachments: A list consisting of all the attachments in the email
        emailmessage: The entire emailmessage itself
    
    Returns:
        Returns a string value if it succeeds or if there is an error, returns None
    
    """
    parser = TRIP_PARSER_MAPPER.get(travelcompanyemail)
    json_resp = parser(htmlbody,attachments).get_trip_details()
    parsed_json=json.loads(json_resp)
    if json_resp == "[]" or parsed_json["Trips"]==[]:
        tripparsingfailed= EmailManagement(uuid.uuid4(),"TripParsingFailedAdmin:"+str(UUID),TRIPFAIL,emailmessage,"TripAutoCreate Failed: Please create the trip manually for the customer.",UUID)
        tripparsingfailed.start()
        threads.append(tripparsingfailed)
        #sendemail(TRIPFAIL,emailmessage,"TripAutoCreate Failed: Please create the trip manually for the customer.",UUID) 
        print UNABLE_TO_RETREIVE_TRIP_DETAILS
        return 
    print json_resp
    tripcreationstatus=1
    tripcreationstatusmessage=""
    for trip in parsed_json["Trips"]:
        print "*********************************************"
        if not trip["TripFrom"]["LocationCode"]:
            depcode=get_airport_code(trip["TripFrom"]["Location"])
        else:
            depcode=trip["TripFrom"]["LocationCode"]
        if not trip["TripTo"]["LocationCode"]:
            arrcode=get_airport_code(trip["TripTo"]["Location"])
        else:
            arrcode=trip["TripTo"]["LocationCode"]
        data = {}
        data["access_token"]=access_token
        data["d_id"]=depcode
        data["departure_time"]=trip["TripFrom"]["Time"]
        data["a_id"]=arrcode
        data["arrival_time"]=trip["TripTo"]["Time"]
        data["category"]="1"
        data["name"]=trip["Carrier"]["Name"]
        data["number"]=trip["Carrier"]["Number"]
        data["toCity"]=trip["TripTo"]["Location"]
        data["fromCity"]=trip["TripFrom"]["Location"]
        data["PNR"]=trip["PNR"]
        data["search_cat_id"]="0"
        trip_create_resp=call_add_trip_api(data)
        print trip_create_resp
        response_data=json.loads(trip_create_resp)
        if response_data["status"]==0: 
            tripcreationstatus=0
            tripcreationstatusmessage=response_data["message"]
    if tripcreationstatus==1:
        tripcreationsuccessthread= EmailManagement(uuid.uuid4(),"TripCreationSuccessThread:"+str(UUID),customeremail,emailmessage,"Your Trip has been created. You are now ready to POWWOW!!!",UUID)
        tripcreationsuccessthread.start()
        threads.append(tripcreationsuccessthread)
        #sendemail(customeremail,emailmessage,"Your Trip has been created. You are now ready to POWWOW!!!",UUID)
    else:
        tripfailthreadcust= EmailManagement(uuid.uuid4(),"TripFailThreadCust:"+str(UUID),customeremail,emailmessage,"You have already added this trip previously. You are now ready to POWWOW!!!",UUID)
        tripfailthreadcust.start()
        threads.append(tripfailthreadcust)
        tripfailthreadadmin= EmailManagement(uuid.uuid4(),"TripFailThreadAdmin:"+str(UUID),TRIPFAIL,emailmessage,"TripAutoCreate Failed for "+ customeremail+ " due to the following error: " + tripcreationstatusmessage,UUID)
        tripfailthreadadmin.start()
        threads.append(tripfailthreadadmin)
        #sendemail(customeremail,emailmessage,"You have already added this trip previously. You are now ready to POWWOW!!!",UUID)
        #sendemail(TRIPFAIL,emailmessage,"TripAutoCreate Failed for "+ customeremail+ " due to the following error: " + tripcreationstatusmessage,UUID) 

    return "COMPLETE"


def print_email_processing_time(emailprocessingstartime):
    """ Prints the amount of time the email processing took. Useful in troubelshooting slow code
    
    Args:
        emailprocessingstarttime: The timestamp in milliseconds when the email processing started
    
    Returns:
        It does not return anything
    
    """
    emailprocessingendtime=datetime.datetime.now()
    print "EMAIL PROCESSING TOOK " + str(emailprocessingendtime-emailprocessingstartime)

        

def main():
    """ You can think of this as the parent module where the execution starts and ends. This method calls all the other methods in this code base.
    
    Args:
        None
    
    Returns:
        It does not return anything
    
    """
    print "UUID: " + str(UUID) # We create a unique identifier for each run so that we can easily identify which run we are looking for in the logs
    emailmessages=[]
    emailmessages=read_tripmailbox("Inbox")
    print "Number of Messages picked up: " + str(len(emailmessages))
    if emailmessages==[]:
        print "No new emails to process"
        return
    for emailmessage in emailmessages:
        emailprocessingstartime=datetime.datetime.now()
        try:            
            msg=email.message_from_string(emailmessage)
            print "*****************************************************"
            print "*****************************************************"
            print "BEGIN PROCESSING FOR CURRENT EMAIL with Subject: " + msg.get("Subject") + " at " + str(datetime.datetime.now())
            htmlbody,attachments=extract_email_contents(emailmessage)
            access_token,customeremail=extract_validate_customeremail(emailmessage,attachments,UUID)
            if not access_token: 
                customeremailinvalidcust= EmailManagement(uuid.uuid4(),"CustomerEmailInvalidCust:"+str(UUID),customeremail,emailmessage,"TripAutoCreate Failed: Please forward the confirmation email from the email address in your powwow profile ",UUID)
                customeremailinvalidcust.start()
                customeremailinvalidadmin= EmailManagement(uuid.uuid4(),"CustomerEmailInvalidAdmin:"+str(UUID),TRIPFAIL,emailmessage,"TripAutoCreate Failed due to mismatched email address. Please contact the customer to help them out.",UUID)
                customeremailinvalidadmin.start()
                print "CustomerEmailAddress Validation failed"
                print_email_processing_time(emailprocessingstartime)
                threads.append(customeremailinvalidcust)
                threads.append(customeremailinvalidadmin)
                continue
            if not(check_worldmate_responsefailure(emailmessage,customeremail,UUID)): 
                worldmateresponsefail= EmailManagement(uuid.uuid4(),"WorldMateResponseFail:"+str(UUID),TRIPFAIL,emailmessage,"WorldMate was not able to parse Trip for user : " + customeremail,UUID)
                worldmateresponsefail.start()
                threads.append(worldmateresponsefail)
                print_email_processing_time(emailprocessingstartime)
                continue
            trvlemailstatus,travelcompanyemail=extract_travelcompanyemail(htmlbody,customeremail,emailmessage)
            print "customeremail:" + customeremail
            print "travelcompany:" + travelcompanyemail
            print "trvlemailstatus: " + trvlemailstatus # 0 = worldmate block list, 1= internal parsing, 2 = forward to worldmate
            if trvlemailstatus=="0":
                print "Travelcompanyemail " + travelcompanyemail + " not supported by worldmate. Sending failure email to TripFail" 
                worldmatenotsupported= EmailManagement(uuid.uuid4(),"WorldMateNotSuported:"+str(UUID),TRIPFAIL,emailmessage,"WorldMate does not support travelcompany: " + travelcompanyemail+ " , customeremail is " + customeremail ,UUID)
                worldmatenotsupported.start()
                threads.append(worldmatenotsupported)
                print_email_processing_time(emailprocessingstartime)
                continue
            if trvlemailstatus=="2":
                print "Travelcompanyemail " + travelcompanyemail + " not mapped. So forwarding to WorldMate" 
                sendemailthread = EmailManagement(uuid.uuid4(),"SendEmailToWorldMate:"+str(UUID),WORLDMATEEMAIL,emailmessage,"CUSTEMAILSTART:"+customeremail+"CUSTEMAILEND:"+"TRAVELCOMPANYEMAILSTART:"+travelcompanyemail+"TRAVELCOMPANYEMAILEND:",UUID)
                sendemailthread.start()
                threads.append(sendemailthread)
                print_email_processing_time(emailprocessingstartime)
                continue
            print "Travelcompanyemail " + travelcompanyemail + " has an internal parser."
            get_trip_details(travelcompanyemail,htmlbody,access_token,customeremail,attachments,emailmessage)
            emailprocessingendtime=datetime.datetime.now()
            print "EMAIL PROCESSING TOOK " + str(emailprocessingendtime-emailprocessingstartime)
        except Exception as e:
            print "Unexpected error: ", str(e)
            traceback.print_exc()
            print "Reached outermost exception handler"
            print_email_processing_time(emailprocessingstartime)
            continue

            
def lambdahandler(event,context):
    """ This is the starting point of the code when called by the AWS Lambda function that is involed every minute by AWS. 
    This function calls the main() method which is defined above.
    
    Args:
        event: This is something passed by the AWS Lambda function and we can ignore it
        context: This is something passed by the AWS Lambda function and we can ignore it
    
    Returns:
        It does not return anything
    
    """
    main()
    for t in threads: # This code waits for all the threads to complete before ending the program execution
        t.join()

if __name__ == '__main__':
    """ This is the starting point of the code when calling the main.py python script from the command line when testing locally.
    
    Args:
        It does not have any arguments.
    
    Returns:
        It does not return anything.
    
    """
    print "worldmateblocklist : " + os.environ['WORLDMATEBLOCKLIST']
    main()
    for t in threads: # This code waits for all the threads to complete before ending the program execution
        t.join()
