import json
import email
import base64
import quopri
import logging
import calendar
import responses
import pprint
from StringIO import StringIO
import traceback

"""
Sample response from the service:
{
"APIVersion": 0.1,
"RequestTimeUTC": "2016-09-14 04:06:15",
"Trips": [{
            "PNR": "RE4WX",

            "Carrier": {
                "Name": "Indigo",
                "Number": "6E 334"
            },

            "Passengers": [
                {
                    "FirstName": "Shyam",
                    "LastName": "Gopal",
                    "Salutation": "Mr"
                },
                {
                    "FirstName": "Radha",
                    "LastName": "Gopal",
                    "Salutation": "Mrs"
                }
            ],

            "TripFrom": {
                "LocationCode": "MAA",
                "LocationName": "Chennai",
                "Time": "21/2/15 12:20",
            },

            "TripTo": {
                "LocationCode": "BLR",
                "LocationName": "Bangalore",
                "Time": "21/2/15 13:15",
            }
        },
        {
            "PNR": "RE4WX",

            "Carrier": {
                "Name": "Indigo",
                "Number": "6E 535"
            },

            "Passengers": [
                {
                    "FirstName": "Shyam",
                    "LastName": "Gopal",
                    "Salutation": "Mr"
                },
                {
                    "FirstName": "Radha",
                    "LastName": "Gopal",
                    "Salutation": "Mrs"
                }
            ],

            "TripTo": {
                "LocationCode": "MAA",
                "LocationName": "Chennai",
                "Time": "24/2/15 14:20",
            },

            "TripFrom": {
                "LocationCode": "BLR",
                "LocationName": "Bangalore",
                "Time": "24/2/15 13:20",
            }
        }]
"""

MONTH_SHORT_NAME_TO_NUM = {v.lower(): str(k).rjust(2, '0') for k, v in enumerate(calendar.month_abbr)}
MONTH_NAME_TO_NUM = {v.lower(): str(k).rjust(2, '0') for k, v in enumerate(calendar.month_name)}


class TripParser(object):
    CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding"
    KNOWN_ENCODINGS = {"base64", "7bit", "quoted"}
    DATE_FMT = "%d/%m/%Y %H:%M"

    @staticmethod
    def _get_passenger_object(first_name, last_name, salutation):
        if not(first_name or last_name):
            ret_val = None
        else:
            salutation = salutation if salutation else ''
            ret_val = responses.Passenger(first_name, last_name, salutation)
        return ret_val

    @staticmethod
    def _get_carrier_object(name, number):
        ret_val = None if not(name or number) else responses.Carrier(name, number)
        return ret_val

    @staticmethod
    def _get_location_object(location, code, event_time):
        ret_val = None if not (location or code) else responses.LocationDetail(location, code, event_time)
        return ret_val

    @staticmethod
    def _get_json_response(passengers, carrier_list, from_loc_list, to_loc_list, pnr_list):
        trips = []
        pnr_len = len(pnr_list)
        trip_len = len(from_loc_list)

        # in case we were not able to get pnr from the email; make dummy blank pnrs in the list
        if not pnr_len:
            pnr_list = [''] * trip_len
        # in case there is just one pnr for a multi leg trip, we just dup the pnrs for the trip length
        elif pnr_len == 1 and trip_len > 1:
            pnr_list *= trip_len
        # in case there are multiple pnrs and multiple trips just duplicate the starting pnr for most of the trip
        elif pnr_len == 2 and trip_len > 2:
            _tmp = pnr_list[1]
            pnr_list = pnr_list[0] * (trip_len - 1)
            pnr_list.append(_tmp)

        for carrier, from_loc, to_loc, pnr in zip(carrier_list, from_loc_list, to_loc_list, pnr_list):
            try:
                pnr = pnr if pnr else ''
                trips.append(responses.Trip(passengers, from_loc, to_loc, carrier, pnr))
            except Exception, ex:
                print "Unable to make response object " + str(ex.message) 
                traceback.print_exc() 
        return json.dumps(responses.TripDetailsResponse(trips).get_jsonable_object())

    @staticmethod
    def _month_abbr_to_int(month):
        return MONTH_SHORT_NAME_TO_NUM.get(month)

    @staticmethod
    def _month_name_to_int(month):
        return MONTH_NAME_TO_NUM.get(month)

    @staticmethod
    def _get_email_body_as_string(message):
        if message.is_multipart():
            messages = []
            for m in message.get_payload():
                if m.is_multipart():
                    messages.append(EmailParser._get_email_body_as_string(m))
                else:
                    messages.append(m.get_payload())
            return ''.join(messages)
        else:
            return message.get_payload()

    def __init__(self, text,attachments):
        self.html_text = text
        self.html_sp = None
        self.xml_flights_node = None
        self.response = "[]"
        if len(attachments)>0:
            self.attachment=attachments[0]
        else: self.attachment=""


    def _construct_response(self):
        blank_list = []
        #try:
            #passengers = self._get_passenger_info() or blank_list
        #except Exception, ex:
            #print "PassengerError " + str(ex.message)
        passengers = blank_list

        try:
            carrier = self._get_carrier_info() or blank_list
        except Exception, ex:
            print "CarrierError " + str(ex.message) 
            traceback.print_exc()
            carrier = blank_list

        try:
            pnr = self._get_reservation_info() or blank_list
        except Exception, ex:
            print "PNRError " + str(ex.message) 
            traceback.print_exc()
            pnr = blank_list

        try:
            from_info, to_info = self._get_location_info()
            from_info = from_info or blank_list
            to_info = to_info or blank_list
        except Exception, ex:
            print "LocationError " + str(ex.message) 
            traceback.print_exc()
            from_info, to_info = blank_list, blank_list

        if carrier and from_info and to_info:
            self.response = self._get_json_response(passengers, carrier, from_info, to_info, pnr)
        #else:
            #self.response = UNABLE_TO_PARSE_EMAIL

    def _get_passenger_info(self):
        raise NotImplementedError

    def _get_location_info(self):
        raise NotImplementedError

    def _get_reservation_info(self):
        raise NotImplementedError

    def _get_carrier_info(self):
        raise NotImplementedError

    def get_trip_details(self):
        return self.response
