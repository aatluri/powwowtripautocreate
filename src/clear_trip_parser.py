# -*- coding: utf-8 -*-
from trip_parser import TripParser
from bs4 import BeautifulSoup


class ClearTripParser(TripParser):
    def __init__(self, text,attachments):
        super(ClearTripParser, self).__init__(text,attachments)
        try:
            self._make_html_soup()
        except Exception as e:
            logging.error(e.message)
        else:
            self._construct_response()

    def _make_html_soup(self):
        if self.html_text:
            self.html_sp = BeautifulSoup(self.html_text, 'html.parser')

    def _get_passenger_info(self):
        if not self.html_sp:
            return

        passenger_list = []
        unique_passengers = set()
        style = "font-size:12px;border-bottom:1px dotted #eeeeee;padding:5px 0 5px 0;color:#000000;" \
                "vertical-align:top;line-height:13px;"
        passengers = self.html_sp.findAll("td", {"style": style})
        if not passengers:
            passengers = self.html_sp.findAll("td", {"style": style.rstrip(';')})

        for passenger in passengers:
            passenger_info = passenger.text.strip().split()
            passenger_info.pop()
            passenger_info.pop()
            last_name = passenger_info.pop()
            salutation = passenger_info.pop(0)
            first_name = " ".join(passenger_info)
            key = first_name + last_name
            if key not in unique_passengers:
                unique_passengers.add(first_name+last_name)
                passenger_list.append(self._get_passenger_object(first_name, last_name, salutation))
        return passenger_list

    def _get_location_info(self):
        if not self.html_sp:
            return

        from_locations = []
        to_locations = []
        style = "font-size:13px;color:#000000;"
        loc_info = self.html_sp.findAll("td", {"style": style})
        if not loc_info:
            loc_info = self.html_sp.findAll("td", {"style": style.rstrip(';')})

        grouped_loc_info = [loc_info[i:i+2] for i in range(0, len(loc_info), 2)]
        for loc_info in grouped_loc_info:
            loc_info[1].find("div").extract()
            date = loc_info[0].find("div").extract().text.split(", ")[1]
            time_from, time_to = loc_info[1].text.split(' ', 1)
            time_to = time_to.split()[-1]
            time_from+=":00"
            time_to+=":00"
            date = date.split(' ')
            day=date[0]
            month = self._month_abbr_to_int(date[1].lower())
            year = "20"+date[2][2:]
            date = year+"-"+month+"-"+day
            departure_time = date + " " + time_from
            arrival_time = date + " " + time_to
            from_loc, to_loc = loc_info[0].text.split(u' → ')
            from_loc, to_loc = from_loc, to_loc
            from_locations.append(self._get_location_object(from_loc, '', departure_time))
            to_locations.append(self._get_location_object(to_loc, '', arrival_time))
        return from_locations, to_locations

    def _get_reservation_info(self):
        if not self.html_sp:
            return

        style = "font-size:11px;color:#999999;display:block;margin-top:2px;"
        pnr_info = self.html_sp.findAll("span", {"style": style})
        if not pnr_info:
            pnr_info = self.html_sp.findAll("span", {"style": style.rstrip(';')})

        unique_pnrs = set()
        pnrs = []
        for pnr in pnr_info:
            if pnr not in unique_pnrs:
                pnrs.append(pnr.find("strong").text)
                unique_pnrs.add(pnr)
        return pnrs

    def _get_carrier_info(self):
        if not self.html_sp:
            return

        carriers = []
        style = "font-size:12px;color:#444444;vertical-align:top;line-height:13px;"
        carrier_info = self.html_sp.findAll("td", {"style": style})
        if not carrier_info:
            carrier_info = self.html_sp.findAll("td", {"style": style.strip(';')})

        for carrier in carrier_info:
            carrier_number = carrier.find("span").extract().text
            carrier_name = carrier.text
            carriers.append(self._get_carrier_object(carrier_name, carrier_number))
        return carriers
