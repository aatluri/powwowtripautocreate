from datetime import datetime
from jsonable import JSONable


class Passenger(JSONable):
    def __init__(self, first_name, last_name, salutation=''):
        self.FirstName = JSONable.string_type(first_name.strip())
        self.LastName = JSONable.string_type(last_name.strip())
        self.Salutation = JSONable.string_type(salutation.strip())


class Carrier(JSONable):
    def __init__(self, name, number=''):
        self.Name = JSONable.string_type(name.strip())
        self.Number = JSONable.string_type(number.strip())


class LocationDetail(JSONable):
    def __init__(self, name, code, time):
        self.Location = JSONable.string_type(name.strip())
        self.LocationCode = JSONable.string_type(code.strip())
        self.Time = JSONable.string_type(time.strip())


class Trip(JSONable):
    def __init__(self, passengers, from_loc, to_loc, carrier, pnr=''):
        #assert len(passengers) > 0, "Trip needs to have at least 1 passenger"
        self.Passengers = JSONable.list_type(passengers)
        self.TripFrom = JSONable.jsonable_type(from_loc)
        self.TripTo = JSONable.jsonable_type(to_loc)
        self.Carrier = JSONable.jsonable_type(carrier)
        self.PNR = JSONable.string_type(pnr.strip())


class TripDetailsResponse(JSONable):
    def __init__(self, trips):
        self.Trips = JSONable.list_type(trips)
        now = datetime.strftime(datetime.utcnow(), "%d/%m/%Y %H:%M:%S")
        self.RequestTimeUTC = JSONable.string_type(now)
        self.APIVersion = JSONable.numeric_type(0.1)
