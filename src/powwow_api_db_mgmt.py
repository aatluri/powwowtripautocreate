import pymysql.cursors
import json
import requests
        


    
    
def get_airport_code(location):
    data={}
    data["search"]=location
    data_json=json.dumps(data)
    headers = {'Content-type': 'application/json'}
    response = requests.post("http://api.powwowtravelapp.com/public/api/get-airports-codes",data=data_json,headers=headers)
    response_string=response.content.decode("utf-8")
    response_data=json.loads(response_string)
    loccode=response_data["airport_codes"][0]["iata_code"]
    return loccode


def call_add_trip_api(data):
    data_json=json.dumps(data);
    print data_json
    headers = {'Content-type': 'application/json'}
    response = requests.post("http://api.powwowtravelapp.com/public/api/add-trip",data=data_json,headers=headers) 
    return response.content.decode("utf-8")    
    

def get_customer_access_token(customeremail):
    print "Validate if CustomerEmail: " + customeremail + " exists in the Powwow database"
    connection = pymysql.connect(host='35.165.120.210',
                         port=3306,
                         user='root',
                         password='abc!@#',
                         db='powwow',
                         charset='utf8mb4',
                         cursorclass=pymysql.cursors.DictCursor)
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `users` where email="+"\'"+customeremail+"\'"
        print sql
        cursor.execute(sql)
        result = cursor.fetchone()
        print "result=" + str(result)
    connection.close()
    if result==None:
        print "Customer Email does not exist in the database"
        return ""
    else:
        print "Customer Email exists in the database"
        access_token=result["access_token"]
        return access_token